FROM alpine:latest
LABEL maintainer = "Gabriel Xavier | https://gabrielxavier.com"

ENV SITE_PATH=/var/www/html/
ENV TZ=America/Sao_Paulo

RUN set -ex; \
    appInstall=" \
        nginx \
        tzdata \
    "; \
    apk update && apk --no-cache add $appInstall; \
    \
    mkdir -p ${SITE_PATH}; \
    cp /usr/share/zoneinfo/${TZ} /etc/localtime && echo "${TZ}" > /etc/timezone;

WORKDIR ${SITE_PATH}

COPY ./webcode/ ${SITE_PATH}
COPY ./config/nginx.conf /etc/nginx/nginx.conf
COPY ./config/site.conf /etc/nginx/conf.d/default.conf

RUN chown -R nginx:nginx ${SITE_PATH} && \
    chmod -R 777 ${SITE_PATH}

EXPOSE 80

CMD ["nginx"]
